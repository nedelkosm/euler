﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;
using Euler.DataStructures;

namespace Euler {
    public static class Problem20 {
        public static string Solve(int n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var factorial = HugeNumbers.Factorial(n);
            var sum = 0L;
            foreach(char c in factorial.ToCharArray() ) {
                sum += int.Parse(c.ToString());
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}