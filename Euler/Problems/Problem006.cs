﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem6 {
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = Series.Sum(1, n);
            var partA = 1.0;
            var partB = Math.Pow(sum, 2);
            for(int i=2 ; i<=n ; i++ ) {
                partA += Math.Pow(i, 2.0);
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return (partB-partA).ToString();
        }
    }
}
