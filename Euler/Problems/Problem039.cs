﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem39 {
        public static string Solve(long n = 1000) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var count = new int[n];
            for (int i=1 ;i<n/2 ;i++ ) {
                for(int k=1 ;k<i ;k++ ) {
                    var hyp = Mathf.Geometry.Triangles.Hypotenuse(i, k);
                    if ( hyp - (int) Math.Floor(hyp) < double.Epsilon ) { // if integer
                        var perimeter = i + k + (int) Math.Floor(hyp);
                        if ( perimeter <= n ) {
                            count[perimeter - 1]++;
                        }
                    }
                }
            }
            var max = 0;
            var value = 0;
            for(int i=0 ; i<count.Length ;i++) {
                if ( count[i] > max ) {
                    max = count[i];
                    value = i+1;
                }
            }
            var result = value.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}