﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem36 {
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0L;
            for (long i=1 ;i<n ; i++ ) {
                if ( Numbers.IsPalindrome(i) && Text.IsPalindrome(Convert.ToString(i,2))) {
                    sum += i;
                }
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}