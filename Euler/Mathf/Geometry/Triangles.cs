﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Mathf.Geometry {
    public static class Triangles {
        public static double Hypotenuse(double sideA, double sideB) {
            return Math.Sqrt(sideA * sideA + sideB * sideB);
        }
    }
}
