﻿using System;
using System.Collections.Generic;
using Euler.Mathf;
using Euler.Utilities;
using System.Linq;

namespace Euler {
    public static class Problem29 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var powers = new List<double>();
            for(int i=2 ;i<=100 ;i++ ) {
                for(int k=2 ;k<=100 ;k++ ) {
                    powers.Add(Math.Pow(i, k));
                }
            }
            powers.Sort();
            var result = powers.Distinct().Count().ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}