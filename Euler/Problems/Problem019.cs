﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;
using Euler.DataStructures;

namespace Euler {
    public static class Problem19 {
        public static string Solve(string date1, string date2) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var tree = new PyramidTree();
            var start = DateTime.Parse(date1);
            var end = DateTime.Parse(date2);
            var count = 0;
            while ( start.CompareTo(end) < 0 ) {
                start = start.AddDays(7);
                if ( start.Day == 1 ) {
                    count++;
                }
            }
            var result = count.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}