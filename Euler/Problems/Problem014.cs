﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler
{
	public static class Problem14
	{
		public static string Solve(long n)
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();

			var maxChain = 0;
			var startingNumber = 0L;
			for (long i = 2; i < n; i++) {
				var chainCount = Series.CollatzChainCount(i);
				if (chainCount > maxChain) {
					maxChain = chainCount;
					startingNumber = i;
				}
			}
			var result = startingNumber.ToString();

			watch.Stop();
			Console.WriteLine(watch.ElapsedMilliseconds + "ms");
			return result;
		}
	}
}