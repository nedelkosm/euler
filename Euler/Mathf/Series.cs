﻿using System;
using System.Collections;

namespace Euler.Mathf {
    public static class Series {
        public static double Product(double[] numbers) {
            var sum = 1.0;
            foreach ( double d in numbers ) {
                sum *= d;
            }
            return sum;
        }
        public static long Product(long[] numbers) {
            var sum = 1L;
            foreach ( long d in numbers ) {
                sum *= d;
            }
            return sum;
        }

        public static long Sum(long from, long to) {
            return (to - from + 1) * (from + to) / 2;
        }

        public static double Fibonacci(double index) {
            if ( index <= 2 ) {
                return index;
            } else {
                return Fibonacci(index - 1) + Fibonacci(index - 2);
            }
        }

        public static long Product(long from, long to) {
            var sum = from;
            for( long i = from+1 ; i<=to ; i++ ) {
                sum *= i;
            }
            return sum;
        }

        public static long Factorial(long number) {
            return Product(1, number);
        }

		public static double[] CollatzChain(double number) {
			var chain = new ArrayList();
			chain.Add(number);
			while (number > 1) {
				if (number % 2 == 0) {
					number = number / 2;
				}
				else {
					number = 3 * number + 1;
				}
				chain.Add(number);
			}
			return chain.ToArray(typeof(double)) as double[];
		}

		public static int CollatzChainCount(long number) {
			var count = 1;
			while (number > 1)
			{
				if (number % 2 == 0)
				{
					number = number / 2;
				}
				else {
					number = 3 * number + 1;
				}
				count++;
			}
			return count;
		}

	}
}
