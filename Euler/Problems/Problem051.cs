﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem051 {
        public static string Solve(uint family) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = "".ToString();

            var prime = 3L;
            while (!IsPrimeFamily(prime,family)) {
                prime = Primes.NextPrime(prime);
                if(prime % 100000 < 10) {
                    Console.WriteLine(prime);
                }
            }

            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static bool IsPrimeFamily(long prime, uint family) {
            Console.WriteLine("-- Candidate: " + prime);
            for(int i=1; i<=10-family;i++){
                var digits = new List<int>(Numbers.Digits(prime));
                var indexes = new List<int>();

                for(int k = 0; k < digits.Count - 1; k++) {
                    if(digits[k] == i) {
                        indexes.Add(i);
                    }
                }
                if(indexes.Count < 1) {
                    break;
                }
                foreach (var ind in Collections.Permutations(indexes)) {
                    var count = 0;
                    if(ind.Count == 0) {
                        continue;
                    }
                    for (int l = 0; l <= 9; l++) {
                        for (int m = 0; m < ind.Count; m++) {
                            digits[ind[m]] = l;
                        }
                        var num = Numbers.LongFromDigit(digits);
                        Console.WriteLine("Searching number " + num);
                        if (Primes.IsPrime(num)) {
                            //Console.WriteLine($"i({i}) l({l}) count({count}) num({num})");
                            count++;
                        }
                    }
                    //Console.WriteLine(prime + " family of " + count);
                    if (count >= family) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}