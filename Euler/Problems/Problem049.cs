﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem49 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sequence = "";
            for (int i=1 ;i<=7 ;i++ ) {
                for(int k=(i+1) ;k<=8 ;k++ ) {
                    for(int j=(k+1) ;j<=9 ;j++ ) {
                        for(int m=1 ;m<=9 ; m++) {
                            var num = i * 1000 + k * 100 + j * 10 + m; // create a 4-digits number with the first 3 digits increasing
                            var permutations = Numbers.Permutations(num); // get all possible permutations of that number
                            permutations.RemoveAll(o => !Primes.IsPrime(o)); // only keep primes
                            for(int p=0 ;p<permutations.Count-2 ; p++ ) { // find suitable prime
                                var selectPrime = permutations[p];
                                if(selectPrime == 1487 ) { // Discard the solution from the example
                                    continue;
                                }
                                var nextPrime = permutations[p + 1];
                                var diff = nextPrime - selectPrime;
                                if ( permutations.Contains(nextPrime + diff) ) {
                                    sequence = "" + selectPrime + nextPrime + (nextPrime + diff);
                                }
                            }
                        }
                    }
                }
            }
            var result = sequence;
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}