﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
	public static class Problem28 {
		public static string Solve(long n) {
			var watch = System.Diagnostics.Stopwatch.StartNew();
			var sum = 1;
			for (int i = 2; i <= n; i++) {
				var currentSum = 16 * i * i - 28 * i + 16;
				sum += currentSum;
			}
			var result = sum.ToString();
			watch.Stop();
			Console.WriteLine(watch.ElapsedMilliseconds + "ms");
			return result;
		}
	}
}