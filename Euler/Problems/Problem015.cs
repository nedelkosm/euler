﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler
{
	public static class Problem15
	{
		public static string Solve(long n)
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();
			var result = Numbers.BinomialCoefficient(2*n, n).ToString();
			watch.Stop();
			Console.WriteLine(watch.ElapsedMilliseconds + "ms");
			return result;
		}
	}
}