﻿using System;
using Euler.Mathf;

namespace Euler {
	public class Problem1 {
		public static string Solve(int input) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = (Multiples(3, input) + Multiples(5, input) - Multiples(15, input)).ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
		}

		private static double Multiples(int num, double max) {
			return Series.Sum(1,((int)Math.Floor((max-1)/num)) * num);
		}
	}
}
