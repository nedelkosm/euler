﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem43 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Dynamic sum = 0;
            var pandigitals = Pandigitals();
            foreach(Dynamic d in pandigitals ) {
                var digits = d.Digits;
                var d234 = digits[1] * 100 + digits[2] * 10 + digits[3];
                if(d234 % 2 != 0 ) {
                    continue;
                }
                var d345 = digits[2] * 100 + digits[3] * 10 + digits[4];
                if ( d345 % 3 != 0 ) {
                    continue;
                }
                var d456 = digits[3] * 100 + digits[4] * 10 + digits[5];
                if ( d456 % 5 != 0 ) {
                    continue;
                }
                var d567 = digits[4] * 100 + digits[5] * 10 + digits[6];
                if ( d567 % 7 != 0 ) {
                    continue;
                }
                var d678 = digits[5] * 100 + digits[6] * 10 + digits[7];
                if ( d678 % 11 != 0 ) {
                    continue;
                }
                var d789 = digits[6] * 100 + digits[7] * 10 + digits[8];
                if ( d789 % 13 != 0 ) {
                    continue;
                }
                var d8910 = digits[7] * 100 + digits[8] * 10 + digits[9];
                if ( d8910 % 17 != 0 ) {
                    continue;
                }
                sum += d;
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static List<Dynamic> Pandigitals() {
            var pandigitals = new List<Dynamic>();
            for ( int d1 = 1 ; d1 <= 9 ; d1++ ) {
                for ( int d2 = 0 ; d2 <= 9 ; d2++ ) {
                    if(d2 == d1 ) {
                        continue;
                    }
                    for ( int d3 = 0 ; d3 <= 9 ; d3++ ) {
                        if ( d3 == d1 || d3 == d2 ) {
                            continue;
                        }
                        for ( int d4 = 0 ; d4 <= 9 ; d4++ ) {
                            if ( d4 == d1 || d4 == d2 || d4 == d3 ) {
                                continue;
                            }
                            for ( int d5 = 0 ; d5 <= 9 ; d5++ ) {
                                if ( d5 == d1 || d5 == d2 || d5 == d3 || d5 == d4 ) {
                                    continue;
                                }
                                for ( int d6 = 0 ; d6 <= 9 ; d6++ ) {
                                    if ( d6 == d1 || d6 == d2 || d6 == d3 || d6 == d4 || d6 == d5 ) {
                                        continue;
                                    }
                                    for ( int d7 = 0 ; d7 <= 9 ; d7++ ) {
                                        if ( d7 == d1 || d7 == d2 || d7 == d3 || d7 == d4 || d7 == d5 || d7 == d6 ) {
                                            continue;
                                        }
                                        for ( int d8 = 0 ; d8 <= 9 ; d8++ ) {
                                            if ( d8 == d1 || d8 == d2 || d8 == d3 || d8 == d4 || d8 == d5 || d8 == d6 || d8 == d7 ) {
                                                continue;
                                            }
                                            for ( int d9 = 0 ; d9 <= 9 ; d9++ ) {
                                                if ( d9 == d1 || d9 == d2 || d9 == d3 || d9 == d4 || d9 == d5 || d9 == d6 || d9 == d7 || d9 == d8 ) {
                                                    continue;
                                                }
                                                for ( int d10 = 0 ; d10 <= 9 ; d10++ ) {
                                                    if ( d10 == d1 || d10 == d2 || d10 == d3 || d10 == d4 || d10 == d5 || d10 == d6 || d10 == d7 || d10 == d8 || d10 == d9 ) {
                                                        continue;
                                                    }
                                                    pandigitals.Add(new Dynamic(""+d1+d2+d3+d4+d5+d6+d7+d8+d9+d10));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return pandigitals;
        }
    }
}