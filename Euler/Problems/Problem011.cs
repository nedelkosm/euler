﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem11 {
        public static string Solve(string file, int digits) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var grid = Text.ParseTable(Files.ReadInput(file), ' ', '|');
            var n = grid.GetLength(0);
            var m = grid.GetLength(1);
            var max = 0.0;
            for ( int i = 0 ; i < n ; i++ ) { // For every digit, check four directions (right, bottom, diagontal-bottom-right, diagontal-botom-left
                for ( int k = 0 ; k < m ; k++ ) {
                    // Right
                    if ( i <= (n - digits) ) {
                        var numbers = new double[digits];
                        for ( int b = 0 ; b < digits ; b++ ) {
                            numbers[b] = grid[i + b, k];
                        }
                        var product = Series.Product(numbers);
                        if ( product > max ) {
                            max = product;
                        }
                    }
                    // Bottom
                    if ( k <= (m - digits) ) {
                        var numbers = new double[digits];
                        for ( int b = 0 ; b < digits ; b++ ) {
                            numbers[b] = grid[i, k + b];
                        }
                        var product = Series.Product(numbers);
                        if ( product > max ) {
                            max = product;
                        }
                    }
                    // Diagonal - right
                    if ( i <= (n - digits) && k <= (m - digits) ) {
                        var numbers = new double[digits];
                        for ( int b = 0 ; b < digits ; b++ ) {
                            numbers[b] = grid[i + b, k + b];
                        }
                        var product = Series.Product(numbers);
                        if ( product > max ) {
                            max = product;
                        }
                    }
                    // Diagonal - left
                    if ( i <= (n - digits) && k >= digits ) {
                        var numbers = new double[digits];
                        for ( int b = 0 ; b < digits ; b++ ) {
                            numbers[b] = grid[i + b, k - b];
                        }
                        var product = Series.Product(numbers);
                        if ( product > max ) {
                            max = product;
                        }
                    }
                }
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return max.ToString();
        }
    }
}