﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem44 {
        public static string Solve(long n = 1000) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var min = 0L;
            var foundMin = false;
            for(long i=2 ;i<=n ; i++ ) {
                for(long k=1 ;k<i ; k++ ) {
                    var pk = Pentagonal(k);
                    var pi = Pentagonal(i);
                    var sum = pk + pi;
                    var diff = pi - pk;
                    if (IsPentagonal(sum) && IsPentagonal(diff) ) {
                        if ( !foundMin ) {
                            foundMin = true;
                            min = diff;
                        } else if(diff < min ) {
                            min = diff;
                        }
                    }
                }
            }
            var result = foundMin? min.ToString() : "No pairs for the first " + n + " numbers.";
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static bool IsPentagonal(long value) {
            var roots = Mathf.Algebra.QuadraticRoots(3, -1, -2 * value);
            foreach(double d in roots ) {
                if(d > 0 && (d-Math.Floor(d)) < double.Epsilon ) { // if positive integer
                    return true;
                }
            }
            return false;
        }

        private static long Pentagonal(long index) {
            return (index * (3 * index - 1)) / 2;
        }
    }
}