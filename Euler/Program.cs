﻿using System;
using Euler.Mathf;
using Euler.Utilities;
using System.Linq;

namespace Euler {
    class MainClass {
        public static string InputDirectory = "../Input/";
        public static void Main(string[] args) {
            //Console.WriteLine(Problem0.Solve());
            //Console.WriteLine(Problem566.Solve());
            //Console.WriteLine(Problem052.Solve(6));
            Console.WriteLine(Problem051.Solve(8));
            //Console.WriteLine(Problem50.Solve());
            //Console.WriteLine(Problem49.Solve());
            //Console.WriteLine(Problem48.Solve(1000));
            //Console.WriteLine(Problem47.Solve());
            //Console.WriteLine(Problem46.Solve());
            //Console.WriteLine(Problem45.Solve());
            //Console.WriteLine(Problem44.Solve(5000));
            //Console.WriteLine(Problem43.Solve());
            //Console.WriteLine(Problem42.Solve("input042.txt"));
            //Console.WriteLine(Problem41.Solve());
            //Console.WriteLine(Problem40.Solve());
            //Console.WriteLine(Problem39.Solve());
            //Console.WriteLine(Problem38.Solve());
            //Console.WriteLine(Problem37.Solve());
            //Console.WriteLine(Problem36.Solve(1000000));
            //Console.WriteLine(Problem35.Solve(1000000));
            //Console.WriteLine(Problem34.Solve());
            //Console.WriteLine(Problem33.Solve());
            //Console.WriteLine(Problem32.Solve());
            //Console.WriteLine(Problem31.Solve(200));
            //Console.WriteLine(Problem30.Solve(5, 355000));
            //Console.WriteLine(Problem29.Solve());
            //Console.WriteLine(Problem28.Solve((1001-1)/2+1));
            //Console.WriteLine(Problem27.Solve(1000, 1000));
            //Console.WriteLine(Problem26.Solve(1000,1));
            //Console.WriteLine(Problem25.Solve(1000));
            //Console.WriteLine(Problem24.Solve("0123456789",1000000));
            //Console.WriteLine(Problem23.Solve());
            //Console.WriteLine(Problem22.Solve("input022.txt"));
            //Console.WriteLine(Problem21.Solve(10000));
            //Console.WriteLine(Problem20.Solve(100));
            //Console.WriteLine(Problem19.Solve("6/1/1901","31/12/2000"));
            //Console.WriteLine(Problem18.Solve("input018.txt"));
            //Console.WriteLine(Problem17.Solve(1000));
            //Console.WriteLine(Problem16.Solve(2,1000));
            //Console.WriteLine(Problem15.Solve(20));
            //Console.WriteLine(Problem14.Solve(1000000));
            //Console.WriteLine(Problem13.Solve(10));
            //Console.WriteLine(Problem12.Solve(500));
            //Console.WriteLine(Problem11.Solve("input011.txt",4));
            //Console.WriteLine(Problem10.Solve(2000000));
            //Console.WriteLine(Problem9.Solve());
            //Console.WriteLine(Problem8.Solve("input008.txt", 13));
            //Console.WriteLine(Problem7.Solve(10001));
            //Console.WriteLine(Problem6.Solve(100));
            //Console.WriteLine(Problem5.Solve(20));
            //Console.WriteLine(Problem4.Solve());
            //Console.WriteLine(Problem3.Solve(600851475143));
            //Console.WriteLine(Problem2.Solve(4000000));
            //Console.WriteLine(Problem1.Solve(1000));
            Console.ReadKey();
        }
    }
}