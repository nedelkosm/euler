﻿using System;
using System.Collections;
using System.Collections.Generic;
using Euler.Utilities;
using System.Linq;

namespace Euler.Mathf {
	public static class Numbers {
        private static Func<int, int[]> divisorsInt = (x => Enumerable.Range(1, (int) x / 2).Where(i => x % i == 0).ToArray());
        public static Func<int, long> Factorial = (k => k == 0 ? 1 : Enumerable.Range(1, k).Aggregate((i, j) => i * j));
        public static int[] Digits(double number){
			var digits = new ArrayList();
			if(number < 0 || !IsInteger(number)){
				throw new ArgumentException("Number must be positive integer");
			}
			while (number >= 10)
			{
				digits.Add((int) (number % 10));
				number = Math.Floor(number/10);
			}
			digits.Add((int)number);
			var digitsArray = new int[digits.Count];
			int count = 0;
			for (int i = digitsArray.Length - 1; i >= 0; i--){
				digitsArray[count++] = (int) digits[i];
			}
			return digitsArray;
		}

		public static bool IsInteger(double number){
			return (number - Math.Floor(number)) < Double.Epsilon;
		}

        internal static long LongFromDigit(List<int> digits) {
            var word = "0";
            foreach(int digit in digits) {
                word += digit.ToString();
            }
            return long.Parse(word);
        }

        public static bool IsPalindrome(long number){
            var digits = Digits(number);
            for ( int i = 0 ; i <= Math.Floor((double) digits.Count / 2) ; i++ ) {
                if ( digits[i] != digits[digits.Count - 1 - i] ) {
                    return false;
                }
            }
            return true;
        }

        // inclusive
        public static bool DividesAll(double number, double divisors) {
            if(!IsInteger(divisors) || !IsInteger(number) ) {
                return false;
            }
            for(int i=2 ; i <= divisors ; i++ ) {
                if(number % i != 0 ) {
                    return false;
                }
            }
            return true;
        }

        public static double Sum(double[] numbers) {
            var sum = 0.0;
            foreach(double d in numbers ) {
                sum += d;
            }
            return sum;
        }

        public static long Sum(long[] numbers) {
            var sum = 0L;
            foreach ( long d in numbers ) {
                sum += d;
            }
            return sum;
        }

        public static long[] Divisors(long number) {
            if ( !IsInteger(number) || number <= 0) {
                throw new ArgumentException("Number must be positive integer");
            }
            var divisors = new ArrayList();
            divisors.Add(1L);
            for ( long i = 2 ; i <= number / 2 ; i++ ) {
                if ( number % i == 0 ) {
                    divisors.Add(i);
                }
            }
            divisors.Add(number);
            return (long[]) divisors.ToArray(typeof(long));
        }

        public static int[] ProperDivisors(int number) {
            return divisorsInt(number);
        }

        public static long[] ProperDivisors(long number) {
            if ( !IsInteger(number) || number <= 0 ) {
                throw new ArgumentException("Number must be positive integer");
            }
            var divisors = new ArrayList();
            divisors.Add(1L);
            for ( long i = 2 ; i <= number / 2 ; i++ ) {
                if ( number % i == 0 ) {
                    divisors.Add(i);
                }
            }
            return (long[]) divisors.ToArray(typeof(long));
        }
        
        public static List<int> Digits(long number) {
            var digits = new List<int>();
            foreach(char c in number.ToString().ToCharArray() ) {
                digits.Add(int.Parse(c.ToString()));
            }
            return digits;
        }

        public static long NumDivisors(long number) {
            if ( !IsInteger(number) || number <= 0 ) {
                throw new ArgumentException("Number must be positive integer");
            }
            var factors = Primes.PrimeFactors(number);
            var occurances = Arrays.BlindOccurances(factors);
            for(int i=0 ;i<occurances.Count ; i++ ) {
                occurances[i] += 1;
            }
            var product = 1L;
            foreach(int i in occurances ) {
                product *= i;
            }
            return product;
        }

		public static long BinomialCoefficient(long top, long bottom) {
			if (bottom > top) {
				return BinomialCoefficient(bottom, top);
			}
			return Series.Factorial(top) / (Series.Factorial(bottom) * Series.Factorial(top - bottom));
		}

		public static bool IsAmicable(long number) {
            var x = Sum(ProperDivisors(number));
            
            if (Sum(ProperDivisors(x)) == number && number != x) {
                return true;
            }
            return false;
        }

        public static bool IsPerfect(long number) {
            return Sum(ProperDivisors(number)) == number;
        }
        public static bool IsDeficient(long number) {
            return Sum(ProperDivisors(number)) < number;
        }
        public static bool IsAbundant(long number) {
            if(number < 12 ) {
                return false;
            }
            return Sum(ProperDivisors(number)) > number;
        }
        public static bool IsPandigital(long number) {
            return IsPandigital(number.ToString());
        }
        public static bool IsPandigital(string number) {
            var chara = number.ToCharArray();
            Array.Sort(chara);
            number = new string(chara);
            if(number == "123456789" ) {
                return true;
            }
            return false;
        }

        public static List<long> Rotations(long number) {
            var rotations = new List<long>();
            var num = number.ToString();
            for ( int i = 0 ; i < num.Length ; i++ ) {
                rotations.Add(long.Parse(num.Substring(i) + num.Substring(0, i)));
            }
            return rotations;
        }

        public static List<long> Permutations(long number) {
            var permutations = new List<long>();
            var combos = Collections.Permutations(Digits(number));
            foreach(List<int> num in combos ) {
                var permutation = "";
                foreach(int digit in num ) {
                    permutation = permutation + digit;
                }
                var newNum = long.Parse(permutation);
                if ( !permutations.Contains(newNum) ) {
                    permutations.Add(newNum);
                }
            }
            return permutations;
        }

    }
}