﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem9 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (int c = 1 ; c < 1000 ; c++ ) {
                for(int b = 1 ; b < c ; b++ ) {
                    for(int a = 1 ; a < b ; a++ ) {
                        if((a*a + b*b) == (c * c) && (a + b + c) == 1000) {
                            watch.Stop();
                            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
                            return (a * b * c).ToString();
                        }
                    }
                }
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return "NoAnswer";
        }
    }
}