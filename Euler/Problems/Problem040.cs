﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem40 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var product = Champernowne(1) * Champernowne(10) * Champernowne(100) * Champernowne (1000)
                * Champernowne(10000) * Champernowne(100000) * Champernowne(1000000);
            var result = product.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static int Champernowne(int index) {
            var count = 0;
            var currentIndex = 0;
            while(currentIndex < index ) {
                count++;
                var digits = Numbers.Digits(count);
                foreach(int digit in digits ) {
                    currentIndex++;
                    if(currentIndex == index ) {
                        return digit;
                    }
                }
            }
            return 0;
        } 
    }
}