﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;
using Euler.DataStructures;

namespace Euler {
    public static class Problem22 {
        public static string Solve(string file) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0;
            var words = Files.ReadWords(Files.ReadInput(file));
            Array.Sort(words);
            var weight = 1;
            foreach (string s in words ) {
                foreach(char c in s ) {
                    sum += ((c-'A') + 1) * weight;
                }
                weight++;
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}