﻿using System;
using Euler.Mathf;

namespace Euler {
	public static class Problem4 {
		public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var maxPalindrome = 1;
			for (int i = 999; i >= 100; i--){ // TODO optimize this loop so it prioritizes the maximum product numbers
				for (int k = 999; k >= 100; k--) {
					var product = i * k;
					if (product > maxPalindrome && Numbers.IsPalindrome(product)) {
						maxPalindrome = product;
					}
				}
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return maxPalindrome.ToString();
		}
	}
}