﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem27 {
        public static string Solve(long n, long m) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            n = Math.Abs(n);
            m = Math.Abs(m);
            int max = 0;
            long a = 0L;
            long b = 0L;
            for ( long i = (-n) ; i < n ; i++ ) {
                for ( long k = (-m) ; k <= m ; k++ ) {
                    var index = 0;
                    var quadratic = 0L;
                    do {
                        index++;
                        quadratic = index * index + index * i + k;
                    } while ( Mathf.Primes.IsPrime(quadratic) );
                    if ( index > max ) {
                        a = i;
                        b = k;
                        max = index;
                    }
                }
            }
            var result = (a*b).ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}