﻿using System;

namespace Euler.Utilities {
    public static class Text {
        public static double[,] ParseTable(string table, char rowSeparator, char collumnSeparator) {
            var rows = table.Split(collumnSeparator);
            var n = rows.Length;
            var m = rows[0].Split(rowSeparator).Length;
            var grid = new double[n,m];
            for(int i=0 ;i<n ; i++ ) {
                var numbers = rows[i].Split(rowSeparator);
                for(int k=0 ; k<m ; k++ ) {
                    grid[i, k] = double.Parse(numbers[k]);
                }
            }
            return grid;
        }

        public static string NumberToText(long number) {
            var result = "";
            var billions = (int) ((number/1000000000) % 1000);
            var millions = (int) ((number / 1000000) % 1000);
            var thousands = (int) ((number / 1000) % 1000);
            var rest = (int) ((number) % 1000);
            if (billions > 0 ) {
                result += Number3ToText(billions) + " billion";
                if(millions > 0 || thousands > 0 || rest > 0 ) {
                    result += " ";
                }
            }
            if (millions > 0 ) {
                result += Number3ToText(millions) + " million";
                if(thousands > 0 || rest > 0 ) {
                    result += " ";
                }
            }
            if (thousands > 0 ) {
                result += Number3ToText(thousands);
                result += " thousand";
                if (rest > 0) {
                    result += " ";
                }
            }
            if (rest > 0 ) {
                result += Number3ToText(rest);
            }
            return result;
        }

        private static string Number3ToText(int number) {
            var result = "";
            if (number == 0 ) {
                return DigitToWord(number);
            }
            if (number < 0 ) {
                result = "negative ";
                number = -number;
            }
            var hundreds = (int) (number / 100)%10;
            var decades = (int) (number / 10)%10;
            var monades = (int) number % 10;
            if (hundreds > 0 ) {
                result += DigitToWord(hundreds) + " hundred";
                if(decades > 0 || monades > 0 ) {
                    result += " and ";
                }
            }
            if (decades > 0 ) {
                decades *= 10;
                if(decades == 10 ) {
                    decades += monades;
                }
                result += DigitToWord(decades);
                if(decades > 19 && monades > 0 ) {
                    result += " ";
                }
            }
            if (monades > 0 && !(decades > 10 && decades < 20)) {
                result += DigitToWord(monades);
            }
            return result;
        }

        private static string DigitToWord(int digit) {
            switch ( digit ) {
                case 0: return "zero";
                case 1: return "one";
                case 2: return "two";
                case 3: return "three";
                case 4: return "four";
                case 5: return "five";
                case 6: return "six";
                case 7: return "seven";
                case 8: return "eight";
                case 9: return "nine";
                case 10: return "ten";
                case 11: return "eleven";
                case 12: return "twelve";
                case 13: return "thirteen";
                case 14: return "fourteen";
                case 15: return "fifteen";
                case 16: return "sixteen";
                case 17: return "seventeen";
                case 18: return "eighteen";
                case 19: return "nineteen";
                case 20: return "twenty";
                case 30: return "thirty";
                case 40: return "forty";
                case 50: return "fifty";
                case 60: return "sixty";
                case 70: return "seventy";
                case 80: return "eighty";
                case 90: return "ninety";
            }
            return "";
        }

        public static bool IsPalindrome(string word) {
            var digits = word.ToCharArray();
            for ( int i = 0 ; i <= Math.Floor((double) digits.Length / 2) ; i++ ) {
                if ( digits[i] != digits[digits.Length - 1 - i] ) {
                    return false;
                }
            }
            return true;
        }
    }
}
