﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem25 {
        public static string Solve(int n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = "1";
            var prev1 = "1";
            var prev2 = "1";
            var index = 2;
            while(sum.Length < n ) {
                index++;
                sum = HugeNumbers.Add(prev1, prev2);
                prev2 = prev1;
                prev1 = sum;
                //Console.WriteLine($"F{index}={sum}");
            }
            var result = index.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}