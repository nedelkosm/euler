# Welcome #

Solving the problems at [Project Euler](https://projecteuler.net/) as a hobby and excercise


### Setup ###

* Clone project to an empty folder
* Open solution file (Euler.sln) with any C# project IDE (This project uses Microsoft Visual Studio) 
* Requirements: Latest C# Version
* No database required

### Structure ###

* Each different problem can be found under problems with the name Problem###.cs
* Every problem contains a static function Solve that returns a string with the solution
* The execution of each problem with default parameters can be found in Program.cs

### Contributors ###

* Owner: Miltos Nedelkos (nedelkos at gmail)