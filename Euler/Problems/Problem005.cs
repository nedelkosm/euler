﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem5 {
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var primes = Primes.PrimesBetween(1, n);
            var product = Series.Product(primes);
            var sum = product;
            while(!Numbers.DividesAll(sum, n) ) {
                sum += product;
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return sum.ToString();
        }
    }
}
