﻿using System;
using System.Linq;
using System.Collections.Generic;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem30 {
        public static string Solve(long n, long m) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var totalSum = 0.0;
            for(long i=2 ;i<=m ;i++ ) {
                var sum = 0.0;
                foreach(int d in Numbers.Digits(i) ) {
                    sum += Math.Pow(d, n);
                }
                if(sum == i ) totalSum += i; 
            }
            var result = totalSum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}