﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Euler.Utilities {
    public static class Arrays {
        public static double Max(double[] array) {
            if(array == null ) {
                throw new ArgumentNullException();
            }
            var max = array[0];
            for(int i=1 ; i<array.Length ; i++ ) {
                if(array[i] > max ) {
                    max = array[i];
                }
            }
            return max;
        }
        public static double Max(long[] array) {
            if ( array == null ) {
                throw new ArgumentNullException();
            }
            var max = array[0];
            for ( int i = 1 ; i < array.Length ; i++ ) {
                if ( array[i] > max ) {
                    max = array[i];
                }
            }
            return max;
        }

        public static List<long> BlindOccurances(List<long> array) {
            var occurances = new List<long>();
            var previous = -1L;
            var count = 0L;
            foreach (long d in array ) {
                if(d == previous ) {
                    count++;
                } else {
                    if ( count != 0) {
                        occurances.Add(count);
                    }
                    count = 1L;
                    previous = d;
                }
            }
            occurances.Add(count);
            return occurances;
        }
    }
}
