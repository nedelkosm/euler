﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem34 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0L;
            for (int i=10 ;i<=100000 ;i++ ) {
                var localSum = 0L;
                foreach(int d in Numbers.Digits(i) ) {
                    localSum += Numbers.Factorial(d);
                }
                if(localSum == i ) {
                    sum += localSum;
                }
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}