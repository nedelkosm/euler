﻿using System;
using System.Collections;

namespace Euler.DataStructures {
    public class PyramidTree {
        private ArrayList layers;
        private int currentLayer = 0;
        private int lastCount = 0;

        public PyramidTree() {
            layers = new ArrayList();
        }

        private Node root { get {
                return (Node) ((ArrayList) layers[0])[0];
            } }

        public class Path {
            public double total;
            private ArrayList path;
            public ArrayList TotalPath { get { return path; } }
            public Path() {
                path = new ArrayList();
            }
            public Path(Path p) {
                CopyPath(p);
            }
            public void CopyPath(Path previousPath) {
                path = new ArrayList();
                if ( previousPath == null ) {
                    return;
                }
                foreach ( Object o in previousPath.path ) {
                    path.Add(o);
                }
                total = previousPath.total;
            }
            public void AddNode(Node n) {
                path.Add(n);
                total += n.Container;
            }
        }

        private class Offer {
            private ArrayList offers;

            public Offer() {
                offers = new ArrayList();
            }

            public void AddOffer(Path path) {
                if(offers == null ) {
                    offers = new ArrayList();
                }
                offers.Add(path);
            }

            public Path MakeChoice() {
                if(offers == null ) {
                    return null;
                }

                Path bestPath = null;
                var bestOffer = 0.0;
                foreach(Path o in offers ) {
                    if(o.total > bestOffer) {
                        bestOffer = o.total;
                        bestPath = o;
                    }
                }
                return bestPath;
            }
        }

        public class Node {
            public Node(double container) {
                this.container = container;
            }
            public double GetMaxPathExtensive() {
                if ( left == null && right == null ) {
                    return container;
                }
                if ( left == null ) {
                    return right.GetMaxPathExtensive() + container;
                }
                if ( right == null ) {
                    return left.GetMaxPathExtensive() + container;
                }
                return Math.Max(left.GetMaxPathExtensive(), right.GetMaxPathExtensive()) + container;
            }
            public double GetMaxPathTopDown() {
                var sum = 0.0;
                var nextLeft = left;
                var nextRight = right;
                var currentNode = this;
                do {
                    sum += currentNode.container;
                    if ( currentNode.left == null && currentNode.right == null ) {
                        currentNode = null;
                    } else
                    if ( currentNode.left == null ) {
                        currentNode = right;
                    } else
                    if ( currentNode.right == null ) {
                        currentNode = left;
                    } else {
                        if ( currentNode.left.container >= currentNode.right.container ) {
                            currentNode = currentNode.left;
                        } else {
                            currentNode = currentNode.right;
                        }
                    }
                } while ( currentNode != null );
                return sum;
            }
            public double GetMaxPathBottomUp() {
                var sum = 0.0;
                var nextLeft = parentLeft;
                var nextRight = parentRight;
                var currentNode = this;
                do {
                    //Console.WriteLine("Searching " + ToString);
                    sum += currentNode.container;
                    if ( currentNode.parentLeft == null && currentNode.parentRight == null ) {
                        currentNode = null;
                    } else
                    if ( currentNode.parentLeft == null ) {
                        currentNode = currentNode.parentRight;
                    } else
                    if ( currentNode.parentRight == null ) {
                        currentNode = currentNode.parentLeft;
                    } else {
                        if ( currentNode.parentLeft.container >= currentNode.parentRight.container ) {
                            currentNode = currentNode.parentLeft;
                        } else {
                            currentNode = currentNode.parentRight;
                        }
                    }
                } while ( currentNode != null );
                return sum;
            }

            private double container;
            public double Container { get { return container; } }
            private Offer offers;
            private Node left;
            public Node Left { get { return left; } set { left = value; } }
            private Node right;
            public Node Right { get { return right; } set { right = value; } }
            private Node parentLeft;
            public Node ParentLeft { get { return parentLeft; } set { parentLeft = value; } }
            private Node parentRight;
            public Node ParentRight { get { return parentRight; } set { parentRight = value;} }

            public void ReceiveOffer(Path path) {
                if(offers == null ) {
                    offers = new Offer();
                }
                offers.AddOffer(path);
            }

            public void SendOffers() {
                if(offers == null ) { // should only be the case in root
                    offers = new Offer();
                }
                var bestPath = GetBestPath();

                if (left != null ) {
                    left.ReceiveOffer(bestPath);
                }
                if(right != null ) {
                    right.ReceiveOffer(bestPath);
                }
            }

            public Path GetBestPath() {
                var path = new Path(offers.MakeChoice());
                path.AddNode(this);
                return path;
            }

            public static bool operator >(Node a, Node b) {
                return (a.container - b.container) > 0;
            }
            public static bool operator <(Node a, Node b) {
                return (a.container - b.container) < 0;
            }

            public new string ToString {
                get {
                    var message = Container + " (";
                    if ( left != null ) {
                        message += left.Container;
                    }
                    if ( right != null ) {
                        if ( left != null ) {
                            message += ",";
                        }
                        message += right.Container;
                    }
                    message += ")";
                    return message;
                }
            }
        }
        
        public Path GetMaxPathOffer() {
            if(layers == null ) {
                return null;
            }
            for (int i=0 ;i<layers.Count ;i++ ) {
                var currentLayer = (ArrayList) layers[i];
                if ( i == (layers.Count - 1) ) { // if the last layer
                    Path maxPath = null;
                    for ( int k = 0 ; k < currentLayer.Count ; k++ ) {
                        var node = (Node) currentLayer[k];
                        var path = node.GetBestPath();
                        if(maxPath == null || path.total > maxPath.total) {
                            maxPath = path;
                        }
                    }
                    return maxPath;
                } else {
                    for ( int k = 0 ; k < currentLayer.Count ; k++ ) {
                        var node = (Node) currentLayer[k];
                        node.SendOffers();
                    }
                }
            }
            return null; // should only get here if layers is empty
        }

        public double GetMaxRoute() {
            return root.GetMaxPathExtensive();
        }

        public double GetRouteTopDown() {
            return root.GetMaxPathTopDown();
        }
        
        public double GetMaxRouteBottomUp() {
            var lastLayer = (ArrayList) layers[layers.Count - 1];
            Node maxNode = null;
            var max = 0.0;
            foreach(Node d in lastLayer ) {
                if(d.Container >= max ) {
                    maxNode = d;
                    max = d.Container;
                }
            }
            return maxNode.GetMaxPathBottomUp();
        }

        public void Insert(double number) {
            if ( currentLayer >= layers.Count ) {
                layers.Add(new ArrayList());
            }
            var cLayer = (ArrayList) layers[currentLayer];
            if ( cLayer.Count == lastCount + 1 ) { // if this layer is full, switch to the next
                layers.Add(new ArrayList());
                currentLayer++;
                lastCount++;
                cLayer = (ArrayList) layers[currentLayer];
            }
            var index = cLayer.Count;
            var newNode = new Node(number);
            cLayer.Add(newNode);
            if ( currentLayer > 0 ) { // If beyond first layer
                var upperLayer = (ArrayList) layers[currentLayer - 1];
                if ( index < upperLayer.Count ) {
                    var parentRight = (Node) upperLayer[index];
                    parentRight.Left = newNode;
                    newNode.ParentRight = parentRight;
                }
                if ( index > 0 ) {
                    var parentLeft = (Node) upperLayer[index - 1];
                    parentLeft.Right = newNode;
                    newNode.ParentLeft = parentLeft;
                }
            }
        }

        public void PrintTree() {
            foreach ( ArrayList layer in layers ) {
                if ( layer.Count > 0 ) {
                    foreach ( Node n in layer ) {
                        Console.Write(n.Container + " ");
                    }
                    Console.WriteLine();
                }
            }
        }
        public void PrintTreeDetails() {
            foreach ( ArrayList layer in layers ) {
                if ( layer.Count > 0 ) {
                    foreach ( Node n in layer ) {
                        Console.Write(n.ToString);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}