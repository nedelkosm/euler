﻿using System;
using System.Collections;
using System.Text;

namespace Euler.Utilities {
    public static class Files {
        public static string ReadInput(string filename) {
            return System.IO.File.ReadAllText(MainClass.InputDirectory + filename);
        }

        public static string[] ReadWords(string fileContents) {
            var words = new ArrayList();
            var wordStrings = fileContents.Replace(System.Environment.NewLine, " ").Replace('"',' ').Replace(',', ' ').Replace('\n', ' ').Replace("  ", " ").Trim().Split(' ');
            foreach ( string s in wordStrings ) {
                if(s.Length >= 1 ) {
                    words.Add(s);
                }
            }
            return (string[]) words.ToArray(typeof(string));
        } 

        public static double[] ReadNumbers(string fileContents) {
            var numbers = new ArrayList();
            var numberStrings = fileContents.Replace(System.Environment.NewLine, " ").Replace('\n', ' ').Replace("  ", " ").Trim().Split(' ');
            foreach(string s in numberStrings ) {
                try {
                    numbers.Add(double.Parse(s));
                } catch(Exception e) {
                    Console.WriteLine($"Could not parse {s}. {e.Message}");
                }
            }
            return (double[]) numbers.ToArray(typeof(double));
        }
    }
}
