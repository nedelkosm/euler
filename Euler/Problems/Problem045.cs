﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem45 {
        public static string Solve(long n = 40755) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var value = n;
            var index = HexagonalIndex(n);
            do {
                index++;
                value = Hexagonal(index);
            } while (!(IsPentagonal(value) && IsTriangular(value) && IsHexagonal(value) && value > n));
            var result = value.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
        private static long Hexagonal(long index) {
            return index * (2 * index - 1);
        }
        private static long HexagonalIndex(long value) {
            return (long) Math.Floor(Mathf.Algebra.QuadraticRoots(2, -1, -value)[0]);
        }
        private static bool IsPentagonal(long value) {
            var roots = Mathf.Algebra.QuadraticRoots(3, -1, -2 * value);
            foreach ( double d in roots ) {
                if ( d > 0 && (d - Math.Floor(d)) < double.Epsilon ) { // if positive integer
                    return true;
                }
            }
            return false;
        }
        private static bool IsTriangular(long value) {
            var roots = Mathf.Algebra.QuadraticRoots(1, 1, -2 * value);
            foreach ( double d in roots ) {
                if ( d > 0 && (d - Math.Floor(d)) < double.Epsilon ) { // if positive integer
                    return true;
                }
            }
            return false;
        }
        private static bool IsHexagonal(long value) {
            var roots = Mathf.Algebra.QuadraticRoots(2, -1, -value);
            foreach ( double d in roots ) {
                if ( d > 0 && (d - Math.Floor(d)) < double.Epsilon ) { // if positive integer
                    return true;
                }
            }
            return false;
        }
    }
}