﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem42 {
        public static string Solve(string file = "input042.txt") {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var words = Files.ReadWords(Files.ReadInput(file));
            var count = 0;
            foreach ( string word in words ) {
                if ( IsTriangle(word) ) {
                    count++;
                }
            }
            var result = count.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
        private static bool IsTriangle(string word) {
            var value = WordValue(word);
            var roots = Algebra.QuadraticRoots(1, 1, -2 * value);
            foreach ( double root in roots ) {
                if ( root - Math.Floor(root) < double.Epsilon ) { // if integer solution
                    return true;
                }
            }
            return false;
        }
        private static int WordValue(string word) {
            var count = 0;
            foreach ( char c in word.ToCharArray() ) {
                count += ((int) (c - 'A') + 1);
            }
            return count;
        }
    }
}