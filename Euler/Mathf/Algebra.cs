﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Mathf {
    public static class Algebra {
        public static double[] QuadraticRoots(double a, double b, double c) {
            var roots = new double[2];
            var d = Math.Sqrt(b * b - 4 * a * c);
            roots[0] = (d - b) / (2 * a);
            roots[1] = (-d - b) / (2 * a);
            return roots;
        }
    }
}
