﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Euler.Mathf {
    public static class Primes {
        public static List<long> PrimeFactors(long num) {
            var composites = new List<long>();
            var remainder = num;
            while ( !IsPrime(remainder) ) {
                var counter = 1L;
                do {
                    counter = NextPrime(counter);
                    if(counter > remainder ) {
                        break;
                    }
                } while ( remainder % counter != 0 );
                composites.Add(counter);
                remainder = (long) (remainder / counter);
            }
            composites.Add(remainder);
            return composites;
        }

        public static long PreviousPrime(long num) {
            if(num <= 2 ) { // If a negative or the result is the lowest prime, return the lowest prime
                return 1;
            }
            if(num % 2 == 0 ) { // If an even number, decrease by 1
                num -= 1;
                if ( IsPrime(num) ) { // if that happens to be a prime, return it
                    return num;
                }
            }
            do {
                if ( num > 3 ) { // Exception for number 2
                    num -= 2;
                } else {
                    return 2;
                }
            } while ( !IsPrime(num) ); // Keep decreasing by 2 until a prime
            return num;
        }

        public static long NextPrime(long num) {
            if(num < 1 ) { // If a non-positive number, return the first prime
                return 1;
            }
            if(num == 1 ) { // Special case
                return 2;
            }
            if(num % 2 == 0 ) { // If an even number, increase by 1
                num += 1;
                if ( IsPrime(num) ) { // if that happens to be a prime, return it
                    return num;
                }
            }
            do {
                num += 2; if (IsPrime(num)) return num;
            } while (num % 5 != 0);
            num += 2; if (IsPrime(num)) return num;
            do {
                num += 2; if (IsPrime(num)) return num;
                num += 2; if (IsPrime(num)) return num;
                num += 2; if (IsPrime(num)) return num;
                num += 4;
            } while ( !IsPrime(num) ); // Keep increasing by 2 until we hit a prime
            return num;
        }

        public static bool IsPrime(long num) {
            if (num < 2 ) {
                return false;
            }
            if (num == 2 || num == 1 ) {
                return true;
            }
            if (num % 2 == 0 || num % 5 == 0) {
                return false;
            }
            for(long i = 3 ; i<=Math.Sqrt(num) ; i+=2 ) {
                if(num % i == 0 ) {
                    return false;
                }
            }
            return true;
        }

        // Exclusive
        public static long[] PrimesBetween(long a, long b) {
            var primes = new ArrayList();
            while(a < b) {
                var nextPrime = NextPrime(a);
                if(nextPrime < b ) {
                    primes.Add(nextPrime);
                }
                a = nextPrime;
            }
            return (long[]) primes.ToArray(typeof(long));
        }
        
        public static long Nth(long n) {
            var count = 1;
            var prime = 2L;
            while ( count != n ) {
                prime = Primes.NextPrime(prime);
                count++;
            }
            return prime;
        }
    }
}
