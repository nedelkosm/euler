﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem35 {
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var count = 13;
            var prime = 100L;
            while(prime < n ) {
                prime = Primes.NextPrime(prime);
                var rotations = Numbers.Rotations(prime);
                bool arePrimes = true;
                for (int r = 1 ; r<rotations.Count ;r++ ) {
                    if ( !Primes.IsPrime(rotations[r]) ) {
                        arePrimes = false;
                        break;
                    }
                }
                if ( arePrimes ) {
                    count++;
                }
            }
            var result = count.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}