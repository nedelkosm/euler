﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

namespace Euler {
    public static class Problem48 {
        public static string Solve(int n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Console.WriteLine("Finding the first " + n.ToString() + " power sums.");
            BigInteger sum = 0;
            for ( int i = 1 ; i <= n ; i++ ) {
                sum += BigInteger.Pow(i,i);
            }
            var result = sum.ToString().Substring(sum.ToString().Length-10);
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
        
        [Obsolete]
        public static string SolveDynamic(Dynamic n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Console.WriteLine("Finding the first " + n.ToString() + " power sums.");
            Dynamic sum = 0;
            for (Dynamic i=new Dynamic(1) ;i<=n ; i++ ) {
                sum += i.Power(i);
                Console.WriteLine($"F({i})= {sum.DigitCount} digit-long");
            }
            var digits = sum.Digits.Skip(sum.Digits.Count - 10);
            var result = "";
            foreach(int i in digits ) {
                result += i;
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}