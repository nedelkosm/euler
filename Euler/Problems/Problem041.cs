﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem41 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for ( int one = 7 ; one >= 1 ; one-- ) {
                for ( int two = 7 ; two >= 1 ; two-- ) {
                    if ( two == one )
                        continue;
                    for ( int three = 7 ; three >= 1 ; three-- ) {
                        if ( three == one || three == two )
                            continue;
                        for ( int four = 7 ; four >= 1 ; four-- ) {
                            if ( four == one || four == two || four == three )
                                continue;
                            for ( int five = 7 ; five >= 1 ; five-- ) {
                                if ( five == one || five == two || five == three || five == four )
                                    continue;
                                for ( int six = 7; six >= 1 ; six-- ) {
                                    if ( six == one || six == two || six == three || six == four || six == five )
                                        continue;
                                    for ( int seven = 7 ; seven >= 1 ; seven-- ) {
                                        if ( seven == one || seven == two || seven == three || seven == four || seven == five || seven == six )
                                            continue;
                                        var num = one * 1000000L + two * 100000L + three * 10000L + four * 1000L + five * 100L + six * 10 + seven;
                                        if ( Primes.IsPrime(num) ) {
                                            var r = num.ToString();
                                            watch.Stop();
                                            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
                                            return r;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var result = "not a 7-digit number";
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}