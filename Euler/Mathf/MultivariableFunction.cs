﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Mathf {
    public enum Sign {
        Positive,
        Negative,
        Zero,
        Unknown
    }
    public class MultivariableFunction {
        private double[] coefficients;
        private Sign[] signs;

        public MultivariableFunction(double[] coefficients, Sign[] signs = null) {
            if (coefficients == null || coefficients.Length == 0) {
                throw new ArgumentNullException("No coefficients given.");
            }
            this.coefficients = new double[coefficients.Length];
            SetCoefficients(coefficients);
            if (signs != null) {
                SetSigns(signs);
            }
        }

        public void SetCoefficients(double[] coefficients) {
            if (this.coefficients.Length != coefficients.Length) {
                this.coefficients = new double[coefficients.Length];
                signs = null;
            }
            for (int i = 0; i < coefficients.Length; i++) {
                this.coefficients[i] = coefficients[i];
            }
        }

        public double Evaluate(double[] variables) {
            if (variables == null) {
                throw new ArgumentNullException("No variables given.");
            }

            if (variables.Length != coefficients.Length - 1) {
                throw new ArgumentException("Variables count must be one less than coefficients.");
            }
            var result = 0.0;
            for (int i = 0; i < coefficients.Length - 1; i++) {
                result += variables[i] * coefficients[i];
            }
            result += coefficients[coefficients.Length - 1];
            return result;
        }

        public static MultivariableFunction operator +(MultivariableFunction f1, MultivariableFunction f2) {
            return (f1 - (-f2));
        }

        public static MultivariableFunction operator -(MultivariableFunction f1, MultivariableFunction f2) {
            if (f1 == null || f2 == null) {
                throw new ArgumentNullException("One or both of given functions are null.");
            }
            if (f1.coefficients.Length != f2.coefficients.Length) {
                throw new ArgumentException("Given functions do not have the same amount of variables.");
            }
            var f3Coef = new double[f1.coefficients.Length];
            for (int i = 0; i < f1.coefficients.Length; i++) {
                f3Coef[i] = f1.coefficients[i] - f2.coefficients[i];
            }
            Sign[] nSigns = null;
            if (f1.signs != null && f2.signs != null) {
                if(f1.signs.Length != f2.signs.Length) {
                    throw new ArgumentException("The two functions have different signs.");
                }
                for(int i=0; i < f1.signs.Length; i++) {
                    if(f1.signs[i] != f2.signs[i]) {
                        throw new ArgumentException("The two functions have different signs.");
                    }
                }
                nSigns = f1.signs;
            } else if(f1.signs != null) {
                nSigns = f1.signs;
            } else if(f2.signs != null) {
                nSigns = f2.signs;
            }
            return new MultivariableFunction(f3Coef, nSigns);
        }

        public static MultivariableFunction operator -(MultivariableFunction f) {
            if(f == null || f.coefficients == null) {
                throw new NullReferenceException("Given function is null.");
            }
            var fnCoef = new double[f.coefficients.Length];
            for(int i=0; i < fnCoef.Length; i++) {
                fnCoef[i] = -f.coefficients[i];
            }
            Sign[] fnSigns = null;
            if(f.signs != null) {
                if(f.signs.Length != fnCoef.Length - 1) {
                    throw new ArgumentException("Sign count must be one less than coefficients.");
                }
                fnSigns = f.signs;
            }
            return new MultivariableFunction(fnCoef, fnSigns);
        }

        public void SetSigns(Sign[] signs) {
            if (signs == null || signs.Length != coefficients.Length - 1) {
                throw new ArgumentException("Sign count must be one less than coefficients.");
            }
            if (this.signs == null) {
                this.signs = new Sign[signs.Length];
            }
            for (int i = 0; i < signs.Length; i++) {
                this.signs[i] = signs[i];
            }
        }

        public Sign FindDerivedSign(Sign[] coefficientsSign) {
            SetSigns(coefficientsSign);
            return DerivedSign;
        }

        public Sign DerivedSign {
            get {
                if (signs == null || signs.Length != coefficients.Length - 1) {
                    throw new InvalidOperationException("Signs are not set or have the wrong count.");
                }
                if (Sign != Sign.Unknown) { // If sign can instantly be derived
                    return Sign;
                }
                var hypothesizedSign = Sign.Unknown; // Assume sign is unknown
                for (int i = 0; i < signs.Length; i++) {
                    if (coefficients[i] == 0 || signs[i] == Sign.Zero) { // If product is zero, the sign doesn't matter
                        continue; // Go to the next sign
                    }
                    if (signs[i] == Sign.Unknown) { // If at least one coefficient is unknown, everything is unknown
                        return Sign.Unknown;
                    }
                    var sign = signs[i];
                    if (sign == Sign.Positive) {
                        sign = coefficients[i] > 0 ? Sign.Positive : Sign.Negative;
                    } else {
                        sign = coefficients[i] > 0 ? Sign.Negative : Sign.Positive;
                    }
                    if (hypothesizedSign == Sign.Unknown) { // If no hypothesis yet, assume the first found sign
                        hypothesizedSign = sign;
                    } else if (hypothesizedSign != sign) { // If two (or more) signs are different, it is impossible to derive the sign
                        return Sign.Unknown;
                    } else {
                        continue;
                    }
                }
                var last = coefficients[coefficients.Length - 1];
                if (last == 0) {
                    return hypothesizedSign;
                } else if (last > 0) {
                    if (hypothesizedSign == Sign.Positive) {
                        return hypothesizedSign;
                    } else {
                        return Sign.Unknown;
                    }
                } else {
                    if (hypothesizedSign == Sign.Negative) {
                        return hypothesizedSign;
                    } else {
                        return Sign.Unknown;
                    }
                }
            }
        }

        public Sign Sign {
            get {
                for (int i = 0; i < coefficients.Length - 1; i++) {
                    if (coefficients[i] != 0) {
                        return Sign.Unknown;
                    }
                }
                var last = coefficients[coefficients.Length - 1];
                if (last < 0) {
                    return Sign.Negative;
                } else if (last > 0) {
                    return Sign.Positive;
                } else {
                    return Sign.Zero;
                }
            }
        }

        public string Description {
            get {
                return $"F[{ToString()}] Sign: {Sign}. Derived sign: {DerivedSign}";
            }
        }

        public override string ToString() {
            var result = "";
            var charList = new char[26];
            for (int i = 0; i < 26; i++) {
                charList[i] = (char)('a' + i);
            }
            for (int i = 0; i < coefficients.Length - 1; i++) {
                if (coefficients[i] != 0) {
                    if (result != "" && result[result.Length - 1] != ' ') {
                        result += " ";
                    }
                    if (coefficients[i] > 0 && result != "") {
                        result += "+ ";
                    } else {
                        if (coefficients[i] < 0) {
                            result += "-";
                            if (result != "-") {
                                result += " ";
                            }
                        }
                    }
                    result += Math.Abs(coefficients[i]) + charList[i].ToString();
                }
            }
            var last = coefficients[coefficients.Length - 1];
            if (last != 0) {
                if (last > 0) {
                    result += " + ";
                } else if(result != "") {
                    result += " ";
                }
                result += last;
            }
            return result;
        }
    }
}
