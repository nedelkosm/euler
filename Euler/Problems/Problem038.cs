﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem38 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var max = 0L;
            for ( int k = 1 ; k < 50000 ; k++ ) {
                for ( int i = 2 ; i <= 5 ; i++ ) {
                    var sum = "";
                    for ( int m = 1 ; m <= i ; m++ ) {
                        sum += k * m;
                    }
                    //Console.WriteLine("Checking " + sum);
                    if ( Numbers.IsPandigital(sum) ) {
                        if(long.Parse(sum) > max ) {
                            max = long.Parse(sum);
                        }
                    }
                }
            }
            var result = max.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}