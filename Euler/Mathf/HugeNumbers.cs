﻿using System;
using System.Collections;
using System.Linq;

namespace Euler.Mathf {
    public static class HugeNumbers {
        public static string Multiply(string number, int multiplier) {
            var digits = new ArrayList();
            for(int i=0 ; i < number.Length ; i++ ) { // reversed digits
                digits.Add(int.Parse(number[number.Length - i - 1].ToString()) * multiplier);
            }
            Carry(digits);
            var result = "";
            for(int i=0 ; i < digits.Count ; i++ ) { // reverse again
                result += digits[digits.Count - i - 1];
            }
            return result;
        }

        public static string Fibonacci(int index) {
            if ( index == 1 ) {
                return "0";
            } else if ( index <= 3 ) {
                return "1";
            }
            return Add(Fibonacci(index - 1), Fibonacci(index - 2));
        }

        public static string Add(string number1, string number2) {
            while(number1.Length != number2.Length ) {
                if(number1.Length < number2.Length ) {
                    number1 = "0" + number1;
                } else {
                    number2 = "0" + number2;
                }
            }
            var digits1 = number1.ToCharArray();
            Array.Reverse(digits1);
            var digits2 = number2.ToCharArray();
            Array.Reverse(digits2);
            
            var sumDigits = new ArrayList();
            for ( int i = 0 ; i < digits1.Length ; i++ ) { // The digit sums
                sumDigits.Add(int.Parse(digits1[i].ToString()) + int.Parse(digits2[i].ToString()));
            }
            Carry(sumDigits);
            var result = "";
            for ( int i = 0 ; i < sumDigits.Count ; i++ ) {
                result += sumDigits[sumDigits.Count - 1 - i].ToString();
            }
            return result;
        }

        public static string Add(string[] numbers) {
            var digits = new char[numbers.Length][];
            for ( int i = 0 ; i < numbers.Length ; i++ ) {
                digits[i] = numbers[i].ToCharArray();
                Array.Reverse(digits[i]);
            }
            var remainder = 0;
            var sumDigits = new ArrayList();
            for ( int i = 0 ; i < numbers[0].Length ; i++ ) { // The digit sums
                remainder = 0;
                for ( int k = 0 ; k < digits.Length ; k++ ) {
                    remainder += int.Parse(digits[k][i].ToString());
                }
                sumDigits.Add(remainder);
            }
            Carry(sumDigits);
            var result = "";
            for ( int i = 0 ; i < sumDigits.Count ; i++ ) {
                result += sumDigits[sumDigits.Count - 1 - i].ToString();
            }
            return result;
        }

        public static string Power(int number, int power) {
            var num = "1";
            for (int i=1 ; i<=power ; i++ ) {
                num = Multiply(num, number);
            }
            return num;
        }

        public static string Factorial(int number) {
            var num = "1";
            for(int i=1 ; i <= number ; i++ ) {
                num = Multiply(num, i);
            }
            return num;
        }

        private static ArrayList Carry(ArrayList digits) {
            var index = 0;
            var remainder = 0.0;
            do {
                if ( index < digits.Count ) {
                    remainder = (int) digits[index];
                } else {
                    remainder = 0;
                    break;
                }
                var digit = remainder % 10;
                var carry = (int) Math.Floor(remainder / 10.0);
                digits[index] = digit;
                if ( carry > 0 ) {
                    if ( (index + 1) >= digits.Count ) {
                        digits.Add(0);
                    }
                    digits[index + 1] = (int) digits[index + 1] + carry;
                }
                index++;
            } while ( true );
            return digits;
        }
    }
}
