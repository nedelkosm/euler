﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Euler.Mathf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Mathf.Tests {
    [TestClass()]
    public class MultivariableFunctionTests {
        [TestMethod()]
        public void EvaluateTest() {
            var coef = new double[] { 1, 3, -5.5, 6};
            var variables = new double[] { 3.1, 0, 1.56 };
            var func = new MultivariableFunction(coef);
            var result = 0.52;
            Assert.IsTrue(func.Evaluate(variables) - result < double.Epsilon);
        }

        [TestMethod()]
        public void DerivedSignTest() {
            var f1 = new MultivariableFunction(new double[] { 0, 3 }, new Sign[] { Sign.Negative });
            var f2 = new MultivariableFunction(new double[] { -5, 3 }, new Sign[] { Sign.Unknown });
            var f3 = new MultivariableFunction(new double[] { 2, 1, 3 }, new Sign[] { Sign.Positive, Sign.Positive});
            var f4 = new MultivariableFunction(new double[] { 2, 1, 3 }, new Sign[] { Sign.Negative, Sign.Negative });
            var f5 = new MultivariableFunction(new double[] { 1, 2, 0 }, new Sign[] { Sign.Negative, Sign.Negative });
            var f6 = new MultivariableFunction(new double[] { 2, 1, 0 }, new Sign[] { Sign.Positive, Sign.Negative });
            var f7 = new MultivariableFunction(new double[] { -1, -1, -2 }, new Sign[] { Sign.Negative, Sign.Negative });
            var f8 = new MultivariableFunction(new double[] { 0 });
            var result = f1.DerivedSign == Sign.Positive;
            result = result && f2.DerivedSign == Sign.Unknown;
            result = result && f3.DerivedSign == Sign.Positive;
            result = result && f4.DerivedSign == Sign.Unknown;
            result = result && f5.DerivedSign == Sign.Negative;
            result = result && f6.DerivedSign == Sign.Unknown;
            result = result && f7.DerivedSign == Sign.Unknown;
            result = result && f8.Sign == Sign.Zero;
            Assert.IsTrue(result);
        }
    }
}