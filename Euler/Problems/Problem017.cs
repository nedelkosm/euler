﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem17 {
        public static string Solve(double n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0;
            for ( int i = 1 ; i <= n ; i++ ) {
                var word = Text.NumberToText(i).Replace(" ", "").Trim();
                //Console.WriteLine($"Number {i} contains {word.Length} letters. Reads: {word}");
                sum += word.Length;
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}