﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem37 {
        public static string Solve(long n = 1000000) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0L;
            var prime = 10L;
            while(prime < n ) {
                prime = Primes.NextPrime(prime);
                bool allPrimes = true;
                var truncates = Truncates(prime);
                foreach (long t in truncates ) {
                    if ( !Primes.IsPrime(t) ) {
                        allPrimes = false;
                        break;
                    }
                }
                if ( allPrimes ) {
                    Console.WriteLine($"{prime} is truncatable");
                    sum += prime;
                }
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static IEnumerable<long> Truncates(long n) {
            var tr = new List<long>();
            var num = n.ToString();
            tr.Add(n);
            for(int i=1 ;i<num.Length ;i++ ) {
                tr.Add(long.Parse(num.Substring(i)));
                tr.Add(long.Parse(num.Substring(0,num.Length-i)));
            }
            return tr;
        }
    }
}