﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem26 {
        public static string Solve(long n, long m) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = "";
            var max = 0;
            var maxi = 0;
            for (int i=1 ;i< n ;i++ ) {
                var digits = new ArrayList();
                var remainders = new ArrayList();
                var number = m.ToString();
                for ( int k = 0 ; k < number.Length ; k++ ) { 
                    digits.Add(int.Parse(number[k].ToString()));
                }
                var remainder = 0;
                var index = -1;
                var rest = 0;
                do {
                    index++;
                    remainder = (int) digits[index];
                    var div = (int) (remainder / i);
                    rest = remainder - div * i;
                    if ( digits.Count == (index + 1) ) {
                        digits.Add(0);
                    }
                    digits[index+1] = rest * 10 + (int) digits[index+1];
                    if ( remainders.Contains(rest) || remainder == 0) {
                        remainders.Add(rest);
                        break;
                    }
                    remainders.Add(rest);
                } while (true);
                if(remainder != 0 ) { // if there is a repeated sequel
                    var sequelSize = remainders.Count - remainders.IndexOf(rest) -1;
                    if(sequelSize > max ) {
                        max = sequelSize;
                        maxi = i;
                    }
                }
            }
            result = maxi.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}