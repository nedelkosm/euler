﻿using System;
using Euler.Utilities;
using Euler.Mathf;
using System.Linq;
using System.Collections.Generic;

namespace Euler {
	public static class Problem3 {
		public static string Solve(long input) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = Primes.PrimeFactors(input).Max().ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
		}
	}
}
