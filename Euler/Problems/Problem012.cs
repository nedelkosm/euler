﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem12 {
        public static string Solve(double n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            int index = 1;
            var divisors = 0.0;
            var sum = 0L;
            do {
                index++;
                sum = Series.Sum(1, index);
                divisors = Numbers.NumDivisors(sum);
            } while ( divisors < n );
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return sum.ToString();
        }
    }
}