﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem052 {
        public static string Solve(uint n = 2) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = "".ToString();
            uint count = 10;
            do {
                count++;
            } while (!ContainsAll(count, n));
            result = count.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;

            bool ContainsAll(uint number, uint multipliers)
            {
                var digits = new List<int> (Numbers.Digits(number));
                digits.Sort();
                var word = Alphanumerics.Convert(digits);

                for(int i = 2; i < multipliers; i++) {
                    var d = new List<int>(Numbers.Digits(i * number));
                    d.Sort();
                    if(Alphanumerics.Convert(d) != word) {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}