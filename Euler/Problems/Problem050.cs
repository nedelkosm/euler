﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
	public static class Problem50 {
		public static string Solve(long n = 1000000) {
			var watch = System.Diagnostics.Stopwatch.StartNew();
			var primes = Primes.PrimesBetween(1, n);
			var max = 0;
			var maxP = 0L;
			for (int i = 0; i < primes.Length; i++){
				var p = primes[i];
				var count = 1;
				var sum = p;
				while (sum < n && (i+count) < primes.Length) {
					p = primes[i + count];
					sum += p;
					count++;
					if (count > max && sum < n && Primes.IsPrime(sum)) {
						max = count;
						maxP = sum;
					}
				}
			}
			var result = maxP.ToString();
			watch.Stop();
			Console.WriteLine(watch.ElapsedMilliseconds + "ms");
			return result;
		}
	}
}