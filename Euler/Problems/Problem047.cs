﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem47 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var count = 1;
            do {
                count++;
                if ( Primes.PrimeFactors(count + 3).Distinct().Count() != 4 ) {
                    count += 3;
                    continue;
                } else if ( Primes.PrimeFactors(count + 2).Distinct().Count() != 4 ) {
                    count += 2;
                    continue;
                } else if ( Primes.PrimeFactors(count + 1).Distinct().Count() != 4 ) {
                    count += 1;
                    continue;
                } else if ( Primes.PrimeFactors(count).Distinct().Count() != 4 ) {
                    continue;
                }
                break;
            } while (true); // only needed a loop wrapper - break and continue take care of the problem
            var result = count.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}