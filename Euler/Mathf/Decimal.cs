﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Mathf {
    class Decimal {
        private string number;
        //private long power;

        public Decimal() {
            number = "";
        }

        public Decimal(string number) {
            foreach(char c in number.ToCharArray() ) {
                if(c < '0' && c > '9' && c != '.' ) {
                    throw new FormatException("Decimal can only contain numbers 0..9 and a decimal point ( . )");
                }
            }
            this.number = number;
        }

        public string Add(Decimal number1, Decimal number2) {
            Align(number1, number2);
            return "";
        }

        private void Align(Decimal number1, Decimal number2) {
            Trim(number1);
            Trim(number2);
            var n1 = number1.number;
            var n2 = number2.number;
            var comma1 = n1.IndexOf('.');
            if ( comma1 < 0 ) {
                n1 = n1 + ".";
                comma1 = n1.Length - 1;
            }
            var comma2 = n2.IndexOf('.');
            if ( comma2 < 0 ) {
                n2 = n2 + ".";
                comma2 = n2.Length - 1;
            }
            for ( int i = 0 ; i < (comma1 - comma2) ; i++ ) {
                n2 = "0" + n2;
            }
            for ( int i = 0 ; i < (comma2 - comma1) ; i++ ) {
                n1 = "0" + n1;
            }
            while(n1.Length != n2.Length ) {
                if(n1.Length > n2.Length ) {
                    n2 = n2 + "0";
                } else {
                    n1 = n1 + "0";
                }
            }
            Console.WriteLine("Number1: " + n1);
            Console.WriteLine("Number2: " + n2);
            number1.number = n1;
            number2.number = n2;
        }

        private void Trim(Decimal number) {
            var newNumber = number.number;
            var countZeros = 0;
            foreach(char c in newNumber.ToCharArray() ) {
                if(c == '0' ) {
                    countZeros++;
                } else {
                    break;
                }
            }
            newNumber = newNumber.Substring(countZeros);
            countZeros = 0;
            for(int i=newNumber.Length-1 ; i>=0 ;i-- ) {
                if ( newNumber[i] == '0' ) {
                    countZeros++;
                } else {
                    break;
                }
            }
            newNumber = newNumber.Substring(0, newNumber.Length - countZeros);
            if(newNumber.Length == 0 ) {
                newNumber = "";
            }
            if(newNumber.Length == 1 && newNumber[0] == '.' ) {
                newNumber = "0";
            }
            number.number = newNumber;
        }
    }
}
