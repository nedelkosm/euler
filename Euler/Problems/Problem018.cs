﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;
using Euler.DataStructures;

namespace Euler {
    public static class Problem18 {
        public static string Solve(string file) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var tree = new PyramidTree();
            var numbers = Files.ReadNumbers(Files.ReadInput(file));
            foreach ( double d in numbers ) {
                tree.Insert(d);
            }
            var path = tree.GetMaxPathOffer();
            var result = tree.GetMaxPathOffer().total.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}