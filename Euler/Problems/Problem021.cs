﻿using System;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;
using Euler.DataStructures;

namespace Euler {
    public static class Problem21 {
        public static string Solve(int n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0;
            for(int i=1 ;i<=n ;i++ ) {
                if ( Numbers.IsAmicable(i) ) {
                    sum += i;
                }
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}