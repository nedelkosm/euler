﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;


namespace Euler {
	public static class Problem566 {
		internal enum LogLevel {
			None = 0,
			Report = 1,
			Minimal = 2,
			Detailed = 3,
			Debug = 4,
			StepByStep = 5
		}

		public static string Solve(int n = 53) {
			var watch = System.Diagnostics.Stopwatch.StartNew();
			var result = "".ToString();
			//var random = new Random();

			var totalFlips = 0;

            totalFlips += CountFlips(9, 10, 11, LogLevel.Report); // 60
            totalFlips += CountFlips(9, 10, 12, LogLevel.Report); // ??
            //totalFlips += CountFlips(15, 16, 17); // 785232
            totalFlips += CountFlips(10, 14, 16, LogLevel.Minimal); // 506

			Console.WriteLine("Total " + totalFlips + " flips.");
			watch.Stop();
			Console.WriteLine(watch.ElapsedMilliseconds + "ms");
			return result;
		}

		private static int CountFlips(int a, int b, int c, LogLevel logLevel = LogLevel.None) {
			var cake = new Cake();
			var x = (int)(360.0 / a);
			var y = (int)(360.0 / b);
			var z = (int)(360.0 / Math.Sqrt(c));

			var currentFlipCount = 0;
			var cutIndex = 0.0;
			if (logLevel > LogLevel.None) {
				Logger.Log($"(A,B,C) = ({a},{b},{c}) (X,Y,Z) = ({x},{y},{z})");
			}
			do {
				var cA = cutIndex + x;
				var cB = cA + y;
				var cC = cB + z;
				if (logLevel > LogLevel.Report) {
                    Logger.Log($"Index({cutIndex}) A({cA}) B({cB}) C({cC}) Count({currentFlipCount}) Cuts ({cake.CutCount})");
				}
				cake.Flip(cutIndex, cA, logLevel);
				if (cake.IsWhole()) break;
				cake.Flip(cA, cB, logLevel);
				if (cake.IsWhole()) break;
				cake.Flip(cB, cC, logLevel);
				cutIndex = cC % 360;
				currentFlipCount += 3;
			} while (!cake.IsWhole());
			if (logLevel > LogLevel.None) {
                Logger.Log($">>> Did {currentFlipCount} flips."); 
			}
			return currentFlipCount;
		}

		private class Cake {
            public Cake()
            {
                Cut.Reset();
            }

			public class Cut : IComparable {
				public double degrees;
				public bool leftSide = true;
				public bool rightSide = true;
				public readonly int id;
                private static int idCount = 0;

                public static void Reset()
                {
                    idCount = 0;
                }

				public Cut() {
					id = ++idCount;
				}

				public Cut(Cut c) {
					this.degrees = c.degrees;
					this.leftSide = c.leftSide;
					this.rightSide = c.rightSide;
					this.id = c.id;
				}

				public override bool Equals(object obj) {
					if (obj.GetType() != typeof(Cut)) {
						return false;
					}
					return degrees == ((Cut)obj).degrees;
				}

				public static bool operator >(Cut c1, Cut c2) {
					return c1.degrees > c2.degrees;
				}

				public static bool operator <(Cut c1, Cut c2) {
					return c1.degrees < c2.degrees;
				}

				public override int GetHashCode() {
					return base.GetHashCode();
				}

				public override string ToString() {
					return $"Cut #{id} [{degrees},{leftSide},{rightSide}]";
				}

                public int CompareTo(object obj)
                {
                    var c = (Cut)obj;
                    if (degrees > c.degrees) return -1;
                    if (degrees == c.degrees) return 0;
                    return 1;
                }
            }
			private List<Cut> cuts = new List<Cut>();
			public int CutCount {
				get {
					return cuts.Count();
				}
			}

			public void Cleanup(LogLevel logLevel = LogLevel.None)
            {
                cuts.Sort((x, y) => (int)(x.degrees - y.degrees));
                if (logLevel > LogLevel.Minimal)
                {
                    Logger.Log("Entering cleanup");
                }
                Validate();
                //cuts.RemoveAll(c => c.leftSide == c.rightSide);
                //CutCake(0);
                //cuts = new List<Cut>(cuts.GroupBy(x => x.degrees).Select(g => g.First()));
                if (logLevel > LogLevel.Minimal)
                {
                    Logger.Log("Cleanup");
                }
                Validate();
            }

            public void Validate() {
                if(cuts.Count == 0)
                {
                    return;
                }
				for (int i = 0; i < cuts.Count - 1; i++) {
					var currentCut = cuts[i];
					var nextCut = cuts[i + 1];
					if (currentCut.rightSide != nextCut.leftSide) {
						Print();
                        Logger.Log($"Problem between ({currentCut}) and ({nextCut})");
						throw new Exception("Impossible state: Cuts don't match");
					}
				}
				var firstCut = cuts[0];
				var lastCut = cuts[cuts.Count - 1];
				if (firstCut.leftSide != lastCut.rightSide) {
					Print();
                    Logger.Log($"Problem between ({firstCut}) and ({lastCut})");
					throw new Exception("Impossible state: Cuts don't match");
				}
			}

			public Cut CutCake(double degrees) {
				while (degrees < 0) {
					degrees += 360;
				}
				degrees = degrees % 360;
				var newCut = new Cut() { degrees = degrees, leftSide = true, rightSide = true };
				if (cuts.Contains(newCut)) {
					return cuts.Find(c => c.degrees == degrees);
				}
				var desiredIndex = 0;
				for (int i = 0; i < cuts.Count; i++) {
					var currentCut = (Cut)cuts[i];
					if (currentCut.degrees < newCut.degrees) {
						desiredIndex = i;
						continue;
					} else {
						break;
					}
				}
				if (cuts.Count > 0) {
					var nextIndex = (desiredIndex + 1) % cuts.Count;
					if (nextIndex < 0) {
						nextIndex += cuts.Count;
					}
					var prevCut = cuts[desiredIndex];
					var nextCut = cuts[nextIndex];
					newCut.leftSide = nextCut.leftSide;
					newCut.rightSide = prevCut.rightSide;
				}
				cuts.Add(newCut);
				cuts.Sort((x, y) => (int)(x.degrees - y.degrees));
				return newCut;
			}

			private void ZeroFlip(double start, double end, LogLevel logLevel = LogLevel.None) {
				if (logLevel > LogLevel.Report) {
                    Logger.Log($"*** Zero flip ({start},{end})***");
				}
				if (logLevel > LogLevel.Detailed) {
                    Logger.Log("Before"); Print();
				}

				var limitStart = start;
				var limitEnd = end;

				start -= 360;
				end -= 360;

				var startCut = CutCake(start);
				var endCut = CutCake(end % 360);
				if (logLevel > LogLevel.Detailed) {
                    Logger.Log("Start cut: " + startCut);
                    Logger.Log("End cut: " + endCut);
				}
				var startLeft = startCut.leftSide;
				var startRight = startCut.rightSide;
				var endRight = endCut.rightSide;
				var endLeft = endCut.leftSide;

				var center = (end - start) / 2.0 + start;

				CutCake(start);
				CutCake(end);

				if (logLevel > LogLevel.Detailed) {
                    Logger.Log("After new cuts "); Print();
				}
				var cutsBetween = cuts.Where((c) => c.degrees >= limitStart || c.degrees <= limitEnd %360);
				if (logLevel > LogLevel.Minimal) {
                    Logger.Log($"> Flipping cake at ({start},{end}), center ({center}), size ({end - start}), cuts ({cutsBetween.Count()})");
				}
				var cutIndex = 0;
				foreach (Cut c in cutsBetween) {
					if (logLevel > LogLevel.Debug) {
                        Logger.Log("  Flipping " + c);
					}
					c.degrees = (center + (center - c.degrees));
					while (c.degrees < 0) c.degrees += 360;
					c.degrees = c.degrees % 360;
					if (logLevel > LogLevel.Debug) {
                        Logger.Log("  Now " + c);
					}
					if (c.degrees == start+360) {
						c.leftSide = startLeft;
						c.rightSide = !endLeft;
						if (logLevel > LogLevel.Detailed) {
                            Logger.Log("  is start " + c);
						}
					} else
					if (c.degrees == end) {
						c.leftSide = !startRight;
						c.rightSide = endRight;
						if (logLevel > LogLevel.Detailed) {
                            Logger.Log("  is end " + c);
						}
					} else {
						var oldLeft = c.leftSide;
						var oldRight = c.rightSide;
						c.leftSide = !oldRight;
						c.rightSide = !oldLeft;
					}
					cutIndex++;
				}
				Cleanup(logLevel);
				if (logLevel > LogLevel.Debug) {
                    Logger.Log("After: "); Print();
				}
			}

			public void Flip(double start, double end, LogLevel logLevel = LogLevel.None) {
				if (end > 360) {
					if (start < 360) {
						ZeroFlip(start, end, logLevel);
						return;
					} else {
						if (logLevel > LogLevel.Report) {
                            Logger.Log($"** Plus flip ({start},{end}) **");
						}
						start = start % 360;
						end = end % 360;
					}
				}

				var startCut = CutCake(start);
				var endCut = CutCake(end);
				var startLeft = startCut.leftSide;
				var startRight = startCut.rightSide;
				var endRight = endCut.rightSide;
				var endLeft = endCut.leftSide;

				var center = (end - start) / 2.0 + start;
				var cutsBetween = cuts.Where((c) => c.degrees >= start && c.degrees <= end);
				if (logLevel > LogLevel.Minimal) {
                    Logger.Log($"> Flipping cake at ({start},{end}), center ({center}), size ({end - start}), cuts ({cutsBetween.Count()})");
				}
				var cutIndex = 0;
				foreach (Cut c in cutsBetween) {
					if (logLevel > LogLevel.Debug) {
                        Logger.Log("  Flipping " + c);
					}
					c.degrees = (center + (center - c.degrees));
					if (logLevel > LogLevel.Debug) {
                        Logger.Log("  Now " + c);
					}

					if (c.degrees == start) {
						c.leftSide = startLeft;
						c.rightSide = !endLeft;
						if (logLevel > LogLevel.Detailed) {
                            Logger.Log("  is start " + c);
						}
					} else
				if (c.degrees == end) {
						c.leftSide = !startRight;
						c.rightSide = endRight;
						if (logLevel > LogLevel.Detailed) {
                            Logger.Log("  is start " + c);
						}
					} else {
						var oldLeft = c.leftSide;
						var oldRight = c.rightSide;
						c.leftSide = !oldRight;
						c.rightSide = !oldLeft;
					}
					cutIndex++;
				}
				Cleanup(logLevel);
				if (logLevel >= LogLevel.StepByStep) {
					Print();
				}
			}

			public bool IsWhole() {
				foreach (Cut c in cuts) {
					if (!c.leftSide || !c.rightSide) {
						return false;
					}
				}
				return true;
			}

			public void Print() {
                Logger.Log("Cake state:");
				foreach (Cut c in cuts) {
                    Logger.Log("  " + c);
				}
                Logger.Log("The cake is " + (IsWhole() ? "" : "not") + " whole.");
			}
		}
	}
}