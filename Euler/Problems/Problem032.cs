﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem32 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var nums = new List<long>();
            var pan4 = Pandigitals(4);
            var pan2 = Pandigitals(2);
            foreach (long i in pan4 ) {
                foreach ( long k in pan2 ) {
                    var identity = "" + i + k + i * k;
                    if ( identity.Length != 9 ) { continue; }
                    if ( Numbers.IsPandigital(identity) ) {
                        nums.Add(i * k);
                    }
                }
            }
            var result = nums.Distinct().Sum().ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static long[] Pandigitals(int digits) {
            var nums = new List<long>();
            for ( int one = 1 ; one <= 9 ; one++ ) {
                nums.Add(one);
                if ( digits > 1 ) {
                    for ( int two = 1 ; two <= 9 ; two++ ) {
                        if ( two == one ) { continue; }
                        nums.Add(one * 10 + two);
                        if ( digits > 2 ) {
                            for ( int three = 1 ; three <= 9 ; three++ ) {
                                if ( three == one || three == two ) { continue; }
                                nums.Add(one * 100 + two * 10 + three);
                                if ( digits > 3 ) {
                                    for ( int four = 1 ; four <= 9 ; four++ ) {
                                        if ( four == one || four == two || four == three ) { continue; }
                                        nums.Add(one * 1000 + two * 100 + three * 10 + four);
                                        if ( digits > 4 ) {
                                            for ( int five = 1 ; five <= 9 ; five++ ) {
                                                if ( five == one || five == two || five == three || five == four ) { continue; }
                                                nums.Add(one * 10000 + two * 1000 + three * 100 + four * 10 + five);
                                                if ( digits > 5 ) {
                                                    for ( int six = 1 ; six <= 9 ; six++ ) {
                                                        if ( six == one || six == two || six == three || six == four || six == five ) { continue; }
                                                        nums.Add(one * 100000 + two * 10000 + three * 1000 + four * 100 + five * 10 + six);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return nums.ToArray();
        }
    }
}