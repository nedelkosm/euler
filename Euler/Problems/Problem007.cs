﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem7 {
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = Primes.Nth(n).ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}