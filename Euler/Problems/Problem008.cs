﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem8 {
        public static string Solve(string file, double n) {
            var numbers = Files.ReadInput(file).Replace(Environment.NewLine,"");
            if(n > numbers.Length ) {
                throw new ArgumentException("There must be more than n numbers.");
            }
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var max = 0.0;
            for(int i=0 ; i <= numbers.Length - n ; i++ ) {
                var digitChars = numbers.Substring(i, (int)n).ToCharArray();
                var digits = new double[digitChars.Length];
                for(int k=0 ; k<digits.Length ; k++ ) {
                    digits[k] = double.Parse(digitChars[k].ToString());
                }
                var product = Series.Product(digits);
                if(product > max ) {
                    max = product;
                }
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return max.ToString();
        }
    }
}