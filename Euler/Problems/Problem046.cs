﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem46 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var num = 9;
            do {
                num += 2;
                if ( Primes.IsPrime(num) ) {
                    continue;
                }
            } while ( IsGoldbach(num) );
            var result = num.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        private static bool IsGoldbach(long number) {
            var prime = 1L;
            do {
                prime = Primes.NextPrime(prime);
                if ( IsInteger(Math.Sqrt((number - prime) / 2.0)) ) {
                    return true;
                }
            } while ( prime < number );
            return false;
        }

        private static bool IsInteger(double number) {
            return number - Math.Floor(number) < double.Epsilon;
        }
    }
}