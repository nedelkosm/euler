﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Utilities {
    public static class Alphanumerics {
        public static string Convert(IEnumerable enumerable) {
            var word = "";
            foreach(var obj in enumerable) {
                word += obj.ToString();
            }
            return word;
        }
    }
}
