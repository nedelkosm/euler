﻿using System;
using Euler.Mathf;

namespace Euler{
	public class Problem2 {
		public static string Solve(double n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            double sum = 2;
			int index = 5;
			while(true){
				var fib = Series.Fibonacci(index);
				if (fib < n){
					sum += fib;
					index += 3;
				}
				else {
					break;
				}
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return sum.ToString();
		}
	}
}
