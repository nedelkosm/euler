﻿using Euler.Mathf;
using System;

namespace Euler {
    public static class Problem10 {
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var result = Numbers.Sum(Primes.PrimesBetween(1, n)).ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}