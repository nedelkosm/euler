﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem31 {
        
        public static string Solve(long n) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var count = 1;
            for(int a=0 ;a<=200 ;a++ ) {
                for(int b = 0 ;b<=100 ;b++ ) {
                    for(int c=0 ;c<=40 ;c++ ) {
                        for(int d=0 ;d<=20 ;d++ ) {
                            for(int e=0 ;e<=10 ;e++ ) {
                                for(int f=0 ;f<=4 ;f++ ) {
                                    for(int g=0 ;g<=2 ;g++ ) {
                                        if((a + b*2 + c*5 + d*10 + e*20 + f*50 + g*100) == 200 ) {
                                            count++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var result = count.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}