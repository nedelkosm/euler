﻿using System;
using System.Collections;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem24 {
        public static string Solve(string characters, long m) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var remainder = m - 1;
            var digits = new ArrayList(characters.ToCharArray());
            var permutation = new ArrayList();
            Func<int, long> Factorial = (k => k == 0 ? 1 : Enumerable.Range(1, k).Aggregate((i, j) => i * j));
            while(digits.Count > 0) {
                var fact = Factorial(digits.Count- 1);
                var index = (int) (remainder / fact);
                var nextChar = digits[index];
                permutation.Add(nextChar);
                digits.RemoveAt(index);
                remainder -= index * fact;
            }
            var result = "";
            foreach (char c in permutation){
                result += c;
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}