﻿using System;
using System.IO;

namespace Euler.Utilities
{
    class Logger
    {
        private static string LogDirectory = "../Log/";
        private static string CurrentLogName = "";

        static Logger()
        {
            var now = System.DateTime.Now;
            CurrentLogName = "Log " + String.Format("{0:MM dd hh mm}", now);
            if (File.Exists(LogDirectory + CurrentLogName + ".log")){
                CurrentLogName += " ";
                if(now.Second < 10)
                {
                    CurrentLogName += "0";
                }
                CurrentLogName += now.Second;
                if (File.Exists(LogDirectory + CurrentLogName + ".log"))
                {
                    File.Delete(LogDirectory + CurrentLogName + ".log");
                }
            }
        }

        public static void Log(string message, string logFile = "", bool printToConsole = true)
        {
            var file = CurrentLogName;
            if(logFile != "" && logFile.Length > 0)
            {
                file = logFile;
            }
            if (printToConsole)
            {
                Console.WriteLine(message);
            }
            File.AppendAllText(LogDirectory+file+".log", message+Environment.NewLine);
        }
    }
}
