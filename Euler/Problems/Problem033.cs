﻿using System;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using System.Collections;
using System.Collections.Generic;

namespace Euler {
    public static class Problem33 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for(int i=11 ;i<100 ;i++ ) {
                for(int k=(i+1) ;k<100 ; k++ ) {
                    var correctResult = i / (double) k;
                    var num1 = (int) (i / 10);
                    var num2 = i % 10;
                    var den1 = (int) (k / 10);
                    var den2 = k % 10;
                    var fakeResult = -1.0;
                    if(num2 == 0 && den2 == 0 ) { // trivial
                        continue;
                    }
                    if ( num1 == den1 ) {
                        fakeResult = num2 / (double) den2;
                    } else
                    if ( num1 == den2 ) {
                        fakeResult = num2 / (double) den1;
                    } else
                    if ( num2 == den1 ) {
                        fakeResult = num1 / (double) den2;
                    } else
                    if ( num2 == den2 ) {
                        fakeResult = num1 / (double) den1;
                    }
                    if(correctResult == fakeResult ) {
                        Console.WriteLine($"{i}/{k}={fakeResult}");
                    }
                }
            }
            var result = "100".ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}