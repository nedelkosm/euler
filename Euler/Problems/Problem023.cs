﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Euler.Mathf;
using Euler.Utilities;
using Euler.DataStructures;

namespace Euler {
    public static class Problem23 {
        public static string Solve() {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            Func<int, bool> isAbundant = (x => Enumerable.Range(1, (int) Math.Sqrt(x)).Where(i => x % i == 0).Sum(i => (i != x / i && i != 1) ? (i + x / i) : i) > x);
            var allAbundant = Enumerable.Range(1, 28122).Where(i => isAbundant(i)).ToList();
            List<int> allSums = new List<int>();
            foreach ( int n in allAbundant )
                foreach ( int m in allAbundant )
                    if ( n + m < 28123 )
                        allSums.Add(n + m);
                    else
                        break;
            var sum = Series.Sum(1,28122) - allSums.Distinct().Sum();

            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }

        public static string SolveNonoptimal() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var sum = 0L;
            var abundants = new ArrayList();
            for ( long i = 12 ; i < 28123 ; i++ ) {
                if ( Numbers.IsAbundant(i) ) {
                    abundants.Add(i);
                    Console.WriteLine(i + " is abundant");
                }
            }
            Console.WriteLine("Found " + abundants.Count + " abundant numbers");
            
            for(long i=1 ;i<=28123 ;i++ ) {
                var cansum = false;
                if ( i < (long) abundants[abundants.Count / 2] ) {
                    for ( int k = 0 ; k < abundants.Count ; k++ ) {
                        var ab = (long) abundants[k];
                        if ( Numbers.IsAbundant(i - ab) ) {
                            //Console.WriteLine($"{i} is the sum of {ab} and {i-ab}.");
                            cansum = true;
                            break;
                        }
                    }
                } else {
                    for ( int k = abundants.Count - 1 ; k >= 0 ; k-- ) {
                        var ab = (long) abundants[k];
                        if ( Numbers.IsAbundant(i - ab) ) {
                            //Console.WriteLine($"{i} is the sum of {ab} and {i-ab}.");
                            cansum = true;
                            break;
                        }
                    }
                }
                if ( !cansum ) {
                    sum += i;
                    //Console.WriteLine($"{i} is non-abundant.");
                }
            } 
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}