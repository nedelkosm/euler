﻿using System;
using System.Collections.Generic;

namespace Euler.Utilities {
    public static class Output {
        public static void Print(Array[,] grid) {
            for(int i=0 ;i<grid.GetLength(0) ; i++ ) {
                for(int k=0 ;k<grid.GetLength(1) ; k++ ) {
                        Console.Write(grid[i, k] + " ");
                }
                Console.WriteLine();
            }
        }

        public static void Print<T>(List<T> list) {
            foreach ( object d in list ) {
                Console.Write(d + " ");
            }
            Console.WriteLine();
        }
        
        public static void Print(Array objects) {
            foreach ( object d in objects ) {
                Console.Write(d + " ");
            }
            Console.WriteLine();
        }

        internal static void Print(IEnumerable<long> enumerable) {
            foreach(long l in enumerable ) {
                Console.Write(l + " ");
            }
            Console.WriteLine();
        }
    }
}
