﻿using System;
using Euler.Mathf;
using Euler.Utilities;

namespace Euler {
    public static class Problem16 {
        public static string Solve(int n, int m) {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var power = HugeNumbers.Power(n, m);
            var sum = 0;
            foreach(char c in power.ToCharArray() ) {
                sum += int.Parse(c.ToString());
            }
            var result = sum.ToString();
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");
            return result;
        }
    }
}