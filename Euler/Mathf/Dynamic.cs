﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

// TODO: Overload == and !=, implement IEquatable<T>
// TODO: add subtraction, multiplication, division
namespace Euler.Mathf {
    public struct Dynamic {
        private readonly string number;

        public Dynamic(Dynamic number) {
            this.number = number.number;
        }
        public Dynamic(string number) {
            this.number = number;
        }
        public Dynamic(int number) {
            this.number = number.ToString();
        }
        public Dynamic(long number) {
            this.number = number.ToString();
        }

        public Dynamic Power(Dynamic power) {
            var original = new Dynamic(number);
            var result = new Dynamic(number);
            for(Dynamic i=new Dynamic(2) ;i<=power ; i++ ) {
                result *= original;
            }
            return result;
        }

        public int DigitCount {
            get {
                return number.Length;
            }
        }
        public List<int> Digits {
            get {
                var digits = new List<int>();
                foreach ( char c in number.ToCharArray() ) {
                    digits.Add(int.Parse(c.ToString()));
                }
                return digits;
            }
        }

        public override string ToString() {
            return number;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public override bool Equals(object obj) {
            if ( obj is Dynamic ) {
                var compare = (Dynamic) obj;
                if ( number == compare.number ) {
                    return true;
                }
            }
            return false;
        }

        public static Dynamic operator +(Dynamic c1, Dynamic c2) {
            return new Dynamic(Add(c1.number, c2.number));
        }
        public static bool operator <(Dynamic c1, Dynamic c2) {
            if(c1 == c2 ) {
                return false;
            }
            var digits1 = c1.Digits;
            var digits2 = c2.Digits;
            if ( digits1.Count < digits2.Count) {
                return true;
            }
            if ( digits1.Count > digits2.Count) {
                return false;
            }
            for(int i=0 ; i<digits1.Count ;i++ ) {
                if(digits1[i] > digits2[i] ) {
                    return false;
                } else if (digits1[i] < digits2[i] ) {
                    return true;
                }
            }
            return false; // equal
        }
        public static bool operator >(Dynamic c1, Dynamic c2) {
            return c1==c2? false : !(c1<c2);
        }
        public static bool operator ==(Dynamic c1, Dynamic c2) {
            return c1.number == c2.number;
        }
        public static bool operator !=(Dynamic c1, Dynamic c2) {
            return !(c1 == c2);
        }
        public static Dynamic operator *(Dynamic multiplicant, Dynamic multiplier) {
            if(multiplier < int.MaxValue ) {
                return new Dynamic(Multiply(multiplicant, int.Parse(multiplier.number)));
            }
            return new Dynamic(Multiply(multiplicant, multiplier));
        }
        /*
        public static Dynamic operator *(Dynamic multiplicant, int multiplier) {
            return new Dynamic(Multiply(multiplicant, multiplier));
        }*/
        public static Dynamic operator ++(Dynamic c1) {
            return c1 + 1;
        }
        public static Dynamic operator +(Dynamic c1, int c2) {
            return new Dynamic(Add(c1.number, c2.ToString()));
        }
        public static Dynamic operator +(Dynamic c1, long c2) {
            return new Dynamic(Add(c1.number, c2.ToString()));
        }

        public static implicit operator Dynamic(int v) {
            return new Dynamic(v);
        }

        public static implicit operator Dynamic(long v) {
            return new Dynamic(v);
        }
        public static implicit operator Dynamic(string v) {
            return new Dynamic(v);
        }
        public static implicit operator long(Dynamic v) { // TODO: shorten it
            return long.Parse(v.number);
        }
        public static implicit operator int(Dynamic v) {
            return int.Parse(v.number);
        }
        public static implicit operator double(Dynamic v) {
            return double.Parse(v.number);
        }


        private static ArrayList Carry(ArrayList digits) {
            var index = 0;
            var remainder = 0.0;
            do {
                if ( index < digits.Count ) {
                    remainder = (int) digits[index];
                } else {
                    remainder = 0;
                    break;
                }
                var digit = remainder % 10;
                var carry = (int) Math.Floor(remainder / 10.0);
                digits[index] = digit;
                if ( carry > 0 ) {
                    if ( (index + 1) >= digits.Count ) {
                        digits.Add(0);
                    }
                    digits[index + 1] = (int) digits[index + 1] + carry;
                }
                index++;
            } while ( true );
            return digits;
        }

        private static string Add(string number1, string number2) {
            while ( number1.Length != number2.Length ) {
                if ( number1.Length < number2.Length ) {
                    number1 = "0" + number1;
                } else {
                    number2 = "0" + number2;
                }
            }
            var digits1 = number1.ToCharArray();
            Array.Reverse(digits1);
            var digits2 = number2.ToCharArray();
            Array.Reverse(digits2);

            var sumDigits = new ArrayList();
            for ( int i = 0 ; i < digits1.Length ; i++ ) { // The digit sums
                sumDigits.Add(int.Parse(digits1[i].ToString()) + int.Parse(digits2[i].ToString()));
            }
            Carry(sumDigits);
            var result = "";
            for ( int i = 0 ; i < sumDigits.Count ; i++ ) {
                result += sumDigits[sumDigits.Count - 1 - i].ToString();
            }
            return result;
        }

        public static Dynamic Multiply(Dynamic number, int multiplier) {
            var digits = new ArrayList();
            for ( int i = 0 ; i < number.number.Length ; i++ ) { // reversed digits
                digits.Add(int.Parse(number.number[number.number.Length - i - 1].ToString()) * multiplier);
            }
            Carry(digits);
            var result = "";
            for ( int i = 0 ; i < digits.Count ; i++ ) { // reverse again
                result += digits[digits.Count - i - 1];
            }
            return new Dynamic(result);
        }

        public static Dynamic Multiply(Dynamic number, Dynamic multiplier) {
            var sum = new Dynamic(0);
            for(Dynamic i = 1 ; i <= multiplier ; i++ ) {
                sum += number;
            }
            return sum;
        }
    }
}
